package com.safebear.app;

import com.safebear.app.pages.WelcomePage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Test01_Login extends BaseTest {
    @Test
    public void testLogin() {
        //Step 1 Confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());
        //Step 2 Click on the Login Link to load page
        welcomePage.clickOnLogin();
        //Step 3 Confirm we're on the Login Page
        assertTrue(loginPage.checkCorrectPage());
        //Step 4 Login with valid credentials
        loginPage.login("testuser","testing");
        //Step 5 Check we're on the correct page
        assertTrue(userPage.checkCorrectPage());
    }
}
